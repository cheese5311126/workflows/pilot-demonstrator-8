# Geodynamics to geohazards

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Mainz.png?inline=false" width="100">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/LMU.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Sorbonne-Univ.png?inline=false" width="90">

**Codes:** 

**Derived SCs:** [SC8.1](https://gitlab.com/cheese5311126/simulation-cases/sc8.1), [SC8.2](https://gitlab.com/cheese5311126/simulation-cases/sc8.2)

## Description

The slow deformation of the solid Earth takes place over long timescales, involves large strains and
non-linear viscoelastoplastic rheologies. This deformation governs processes on a wide range of scales, from fluid flowing
through porous rocks on a micrometer scale to the motion of tectonic plates on thousands of kilometers, and controls the
stress state of the lithosphere, affects where earthquakes occur, or how magma migrates through the crust. It also affects
stresses within sedimentary basins and geothermal reservoirs and thus plays a key role for the long-term safety of nuclear
waste deposits. Whereas existing geodynamic codes are typically applied to more basic scientific questions (such as how
mountain belts form), they can equally well be used to simulate geohazards such as landslides or coupled
geodynamic/seismic simulations. This PD focusses on these topics, which requires work to couple geodynamic with seismic
codes.

## Objective

The main objectives of this PD are: (1) improve the workflow of geodynamic codes to such an extent that they
can be rapidly deployed to simulate any part of the Earth and model output can be compared with observations, (2) export
and project material properties and state of stress to other codes for multiphysics modeling purposes and, (3) prepare
geodynamic codes for the next generation GPU machines, by improving multigrid strategies, implement matrix free solvers
and as well as novel BFBt preconditioners.